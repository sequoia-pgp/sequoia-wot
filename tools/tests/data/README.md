With the expection of `gpg-trustroots.pgp`, the files here are copied
from `../../../lib/tests/data`.  (See that directory for
documentation.)  Those files are authoritative; don't modify these
files, modify those and copy over the changes.
