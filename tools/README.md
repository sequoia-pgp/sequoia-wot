Tools for querying a web of trust.

Usually you should use
[`sq`](https://gitlab.com/sequoia-pgp/sequoia-sq), however, these
tools have a few more features including the ability to generate DOT
output, and support for using `gpg`'s certificate store and trust
roots.
