#[macro_use] mod log;

use std::collections::BTreeMap;
use std::collections::btree_map::Entry;
use std::time::SystemTime;

use anyhow::Context;

use chrono::NaiveTime;
use chrono::DateTime;
use chrono::Utc;

use clap::Parser;

use sequoia_openpgp as openpgp;
use openpgp::cert::raw::RawCert;
use openpgp::cert::raw::RawCertParser;
use openpgp::KeyID;
use openpgp::Fingerprint;
use openpgp::KeyHandle;
use openpgp::Result;
use openpgp::packet::UserID;
use openpgp::parse::Parse;
use openpgp::policy::Policy;
use openpgp::policy::StandardPolicy;

use sequoia_policy_config::ConfiguredStandardPolicy;

use sequoia_cert_store as cert_store;
use cert_store::AccessMode;
use cert_store::store::KeyServer;
use cert_store::store::StatusListener;
use cert_store::store::StatusUpdate;
use cert_store::store::StoreError;

use sequoia_wot as wot;
use wot::store::Backend;
use wot::store::CertStore;
use wot::store::Store;
use wot::store::StorePlusBackend;
use crate::output::OutputType;

mod cli;
mod gpg;
mod output;

const TRACE: bool = false;

/// Parses the given string depicting a ISO 8601 timestamp.
///
/// Note: this is copied from sq/src/sq.rs.  If you change something
/// here, also change it there.
fn parse_iso8601(s: &str, pad_date_with: NaiveTime)
                 -> Result<DateTime<chrono::Utc>>
{
    // If you modify this function this function, synchronize the
    // changes with the copy in sqv.rs!
    for f in &[
        "%Y-%m-%dT%H:%M:%S%#z",
        "%Y-%m-%dT%H:%M:%S",
        "%Y-%m-%dT%H:%M%#z",
        "%Y-%m-%dT%H:%M",
        "%Y-%m-%dT%H%#z",
        "%Y-%m-%dT%H",
        "%Y%m%dT%H%M%S%#z",
        "%Y%m%dT%H%M%S",
        "%Y%m%dT%H%M%#z",
        "%Y%m%dT%H%M",
        "%Y%m%dT%H%#z",
        "%Y%m%dT%H",
    ] {
        if f.ends_with("%#z") {
            if let Ok(d) = DateTime::parse_from_str(s, *f) {
                return Ok(d.into());
            }
        } else if let Ok(d) = chrono::NaiveDateTime::parse_from_str(s, *f) {
            return Ok(DateTime::from_utc(d, Utc));
        }
    }
    for f in &[
        "%Y-%m-%d",
        "%Y-%m",
        "%Y-%j",
        "%Y%m%d",
        "%Y%m",
        "%Y%j",
        "%Y",
    ] {
        if let Ok(d) = chrono::NaiveDate::parse_from_str(s, *f) {
            return Ok(DateTime::from_utc(d.and_time(pad_date_with), Utc));
        }
    }
    Err(anyhow::anyhow!("Malformed ISO8601 timestamp: {}", s))
}

// Sometimes the same error cascades, e.g.:
//
// ```
// $ sq-wot --time 20230110T0406   --keyring sha1.pgp path B5FA089BA76FE3E17DC11660960E53286738F94C 231BC4AB9D8CAB86D1622CE02C0CE554998EECDB FABA8485B2D4D5BF1582AA963A8115E774FA9852 "<carol@example.org>"
// [ ] FABA8485B2D4D5BF1582AA963A8115E774FA9852 <carol@example.org>: not authenticated (0%)
//   ◯ B5FA089BA76FE3E17DC11660960E53286738F94C ("<alice@example.org>")
//   │   No adequate certification found.
//   │   No binding signature at time 2023-01-10T04:06:00Z
//   │     No binding signature at time 2023-01-10T04:06:00Z
//   │     No binding signature at time 2023-01-10T04:06:00Z
// ...
// ```
//
// Compress these.
fn error_chain(err: &anyhow::Error) -> Vec<String> {
    let mut errs = std::iter::once(err.to_string())
        .chain(err.chain().map(|source| source.to_string()))
        .collect::<Vec<String>>();
    errs.dedup();
    errs
}

fn trust_amount(cli: &cli::Cli)
    -> Result<usize>
{
    let amount = if let Some(v) = cli.trust_amount {
        v as usize
    } else if cli.full {
        wot::FULLY_TRUSTED
    } else if cli.partial {
        wot::PARTIALLY_TRUSTED
    } else if cli.double {
        2 * wot::FULLY_TRUSTED
    } else {
        if cli.certification_network {
            // Look for multiple paths.  Specifically, try to find 10
            // paths.
            10 * wot::FULLY_TRUSTED
        } else {
            wot::FULLY_TRUSTED
        }
    };

    Ok(amount)
}

// Returns whether there is a matching self-signed User ID.
fn have_self_signed_userid(cert: &wot::CertSynopsis,
                           pattern: &UserID, email: bool)
    -> bool
{
    if email {
        if let Ok(Some(pattern)) = pattern.email_normalized() {
            // userid contains a valid email address.
            cert.userids().any(|u| {
                if let Ok(Some(userid)) = u.userid().email_normalized() {
                    pattern == userid
                } else {
                    false
                }
            })
        } else {
            false
        }
    } else {
        cert.userids().any(|u| u.userid() == pattern)
    }
}

/// Authenticate bindings on a Network.
fn authenticate<S>(
    cli: &cli::Cli,
    n: &wot::Network<S>,
    gossip: bool,
    userid: Option<&UserID>,
    certificate: Option<&KeyHandle>,
) -> Result<()>
    where S: wot::store::Store
{
    let required_amount = trust_amount(cli)?;

    let fingerprint: Option<Fingerprint> = if let Some(kh) = certificate {
        Some(match kh {
            KeyHandle::Fingerprint(fpr) => fpr.clone(),
            kh @ KeyHandle::KeyID(_) => {
                let certs = n.lookup_synopses(kh)?;
                if certs.is_empty() {
                    return Err(StoreError::NotFound(kh.clone()).into());
                }
                if certs.len() > 1 {
                    return Err(anyhow::anyhow!(
                        "The Key ID {} is ambiguous.  \
                         It could refer to any of the following \
                         certificates: {}.",
                        kh,
                        certs.into_iter()
                            .map(|c| c.fingerprint().to_hex())
                            .collect::<Vec<String>>()
                            .join(", ")));
                }

                certs[0].fingerprint()
            }
        })
    } else {
        None
    };

    let email = cli.subcommand.email();

    let mut bindings = Vec::new();
    if matches!(userid, Some(_)) && email {
        let userid = userid.expect("required");

        // First, we check that the supplied User ID is a bare
        // email address.
        let email = String::from_utf8(userid.value().to_vec())
            .context("email address must be valid UTF-8")?;

        let userid_check = UserID::from(format!("<{}>", email));
        if let Ok(Some(email_check)) = userid_check.email() {
            if email != email_check {
                println!("{:?} does not appear to be an email address",
                         email);
                std::process::exit(1);
            }
        } else {
            println!("{:?} does not appear to be an email address",
                     email);
            std::process::exit(1);
        }

        // Now, iterate over all of the certifications of the target,
        // and select the bindings where the User ID matches the email
        // address.
        bindings = if let Some(fingerprint) = fingerprint.as_ref() {
            n.certified_userids_of(fingerprint)
                .into_iter()
                .map(|userid| (fingerprint.clone(), userid))
                .collect::<Vec<_>>()
        } else {
            n.lookup_synopses_by_email(&email)
        };

        let email_normalized = userid_check.email_normalized()
            .expect("checked").expect("checked");
        bindings = bindings.into_iter()
            .filter_map(|(fingerprint, userid_other)| {
                if let Ok(Some(email_other_normalized))
                    = userid_other.email_normalized()
                {
                    if email_normalized == email_other_normalized {
                        Some((fingerprint, userid_other.clone()))
                    } else {
                        None
                    }
                } else {
                    None
                }
            }).collect();
    } else if let Some(fingerprint) = fingerprint {
        if let Some(userid) = userid {
            bindings.push((fingerprint, userid.clone()));
        } else {
            // Fingerprint, no User ID.
            bindings = n.certified_userids_of(&fingerprint)
                .into_iter()
                .map(|userid| (fingerprint.clone(), userid))
                .collect();
        }
    } else if let Some(userid) = userid {
        // The caller did not specify a certificate.  Find all
        // bindings with the User ID.
        bindings = n.lookup_synopses_by_userid(userid.clone())
            .into_iter()
            .map(|fpr| (fpr, userid.clone()))
            .collect();
    } else {
        // No User ID, no Fingerprint.
        // List everything.

        bindings = n.certified_userids();

        if let cli::Subcommand::List { pattern: Some(pattern), .. } = &cli.subcommand {
            // Or rather, just User IDs that match the pattern.
            let pattern = pattern.to_lowercase();

            bindings = bindings
                .into_iter()
                .filter(|(_fingerprint, userid)| {
                    if email {
                        // Compare with the normalized email address,
                        // and the raw email address.
                        if let Ok(Some(email)) = userid.email_normalized() {
                            // A normalized email is already lowercase.
                            if email.contains(&pattern) {
                                return true;
                            }
                        }

                        if let Ok(Some(email)) = userid.email() {
                            if email.to_lowercase().contains(&pattern) {
                                return true;
                            }
                        }

                        return false;
                    } else if let Ok(userid)
                        = std::str::from_utf8(userid.value())
                    {
                        userid.to_lowercase().contains(&pattern)
                    } else {
                        // Ignore User IDs with invalid UTF-8.
                        false
                    }
                })
                .collect();
        }
    };

    // There may be multiple certifications of the same
    // User ID.  Dedup.
    bindings.sort();
    bindings.dedup();

    let mut authenticated = false;
    let mut lint_input = true;

    let mut output: Box<dyn OutputType> = match cli.format {
        #[cfg(feature = "dot-writer")]
        cli::OutputFormat::Dot => {
            Box::new(output::DotOutputNetwork::new(
                required_amount,
                n.roots(),
                gossip,
                cli.certification_network,
            ))
            as Box<dyn output::OutputType>
        }
        cli::OutputFormat::HumanReadable => {
            Box::new(
                output::HumanReadableOutputNetwork::new(required_amount, gossip)
            )
        }
    };

    for (fingerprint, userid) in bindings.iter() {
        let paths = if gossip {
            n.gossip(fingerprint.clone(), userid.clone())
        } else {
            n.authenticate(
                userid.clone(), fingerprint.clone(), required_amount)
        };

        let aggregated_amount = paths.amount();
        if aggregated_amount == 0 && ! gossip {
            continue;
        }
        lint_input = false;
        if gossip {
            authenticated = true;
        } else if aggregated_amount >= required_amount {
            authenticated = true;
        }

        let paths = paths.into_iter().collect::<Vec<(wot::Path, usize)>>();

        output.add_paths(paths, fingerprint, userid, aggregated_amount)?;

    }

    output.finalize()?;

    // We didn't show anything.  Try to figure out what was wrong.
    if lint_input {
        // See if the target certificate exists.
        if let Some(kh) = certificate {
            match n.lookup_synopses(kh) {
                Err(err) => {
                    eprintln!("Looking up target certificate ({}): {}",
                             kh, err);
                }
                Ok(certs) => {
                    for cert in certs.iter() {
                        let fpr = cert.fingerprint();
                        let kh = if certs.len() == 1 {
                            KeyHandle::KeyID(KeyID::from(&fpr))
                        } else {
                            KeyHandle::Fingerprint(fpr.clone())
                        };

                        // Check if the certificate was revoke.
                        use wot::RevocationStatus;
                        match cert.revocation_status() {
                            RevocationStatus::Soft(_)
                            | RevocationStatus::Hard => {
                                eprintln!("Warning: {} is revoked.", kh);
                            }
                            RevocationStatus::NotAsFarAsWeKnow => (),
                        }

                        // Check if the certificate has expired.
                        if let Some(e) = cert.expiration_time() {
                            if e <= n.reference_time() {
                                eprintln!("Warning: {} is expired.", kh);
                            }
                        }

                        // See if there is a matching self-signed User ID.
                        if let Some(userid) = userid {
                            if ! have_self_signed_userid(cert, userid, email) {
                                eprintln!("Warning: {} is not a \
                                          self-signed User ID for {}.",
                                         userid, kh);
                            }
                        }

                        // See if there are any certifications made on
                        // this certificate.
                        if let Ok(css) = n.certifications_of(&fpr, 0.into()) {
                            if css.iter().all(|cs| {
                                cs.certifications().all(|(_userid, certifications)| {
                                    certifications.is_empty()
                                })
                            })
                            {
                                eprintln!("Warning: {} has no valid \
                                          certifications.",
                                         kh);
                            }
                        }
                    }
                }
            }
        }

        // Perhaps the caller specified an email address, but forgot
        // to add --email.  If --email is not present and the
        // specified User ID looks like an email, try and be helpful.
        if ! email {
            if let Some(userid) = userid {
                if let Ok(email) = std::str::from_utf8(userid.value()) {
                    let userid_check = UserID::from(format!("<{}>", email));
                    if let Ok(Some(email_check)) = userid_check.email() {
                        if email == email_check {
                            eprintln!("WARNING: {} appears to be a bare \
                                      email address.  Perhaps you forgot \
                                      to specify --email.",
                                     email);
                        }
                    }
                }
            }
        }

        // See if the trust roots exist.
        if ! gossip {
            if n.roots().iter().all(|r| {
                let fpr = r.fingerprint();
                if let Err(err) = n.lookup_synopsis_by_fpr(&fpr) {
                    eprintln!("Looking up trust root ({}): {}.",
                             fpr, err);
                    true
                } else {
                    false
                }
            })
            {
                eprintln!("No trust roots found.");
            }
        }
    }

    if ! authenticated {
        if ! lint_input {
            eprintln!("Could not authenticate any paths.");
        } else {
            eprintln!("No paths found.");
        }
        std::process::exit(1);
    }

    Ok(())
}

// For `sq-wot path`.
fn check_path<'a, S>(cli: &cli::Cli, q: &wot::Network<S>,
                     policy: &dyn Policy)
    -> Result<()>
where S: wot::store::Store + wot::store::Backend<'a>
{
    tracer!(TRACE, "check_path");

    let required_amount = trust_amount(cli)?;

    let (khs, userid) = if let cli::Subcommand::Path { path, .. } = &cli.subcommand {
        (path.certs()?, path.userid()?)
    } else {
        unreachable!("checked");
    };

    assert!(khs.len() > 0, "guaranteed by clap");

    let r = q.lint_path(&khs, &userid, required_amount, policy);

    let target_kh = khs.last().expect("have one");

    match r {
        Ok(path) => {
            match cli.format {
                #[cfg(feature = "dot-writer")]
                cli::OutputFormat::Dot => {
                    eprintln!(
                        "DOT output for {} path is not yet implemented!",
                        env!("CARGO_BIN_NAME"),
                    );
                }
                cli::OutputFormat::HumanReadable => {
                    crate::output::print_path_header(
                        target_kh,
                        &userid,
                        path.amount(),
                        required_amount,
                    );
                    crate::output::print_path(&path, &userid, "  ");
                }
            };

            if path.amount() >= required_amount {
                std::process::exit(0);
            }
        }
        Err(err) => {
            match cli.format {
                #[cfg(feature = "dot-writer")]
                cli::OutputFormat::Dot => {
                    eprintln!(
                        "DOT output for {} path is not yet implemented!",
                        env!("CARGO_BIN_NAME"),
                    );
                }
                cli::OutputFormat::HumanReadable => {
                    crate::output::print_path_header(
                        target_kh,
                        &userid,
                        0,
                        required_amount,
                    );
                    crate::output::print_path_error(err);
                }
            };
        }
    }

    std::process::exit(1);
}

struct KeyServerUpdate {
}

impl StatusListener for KeyServerUpdate {
    fn update(&self, update: &StatusUpdate) {
        eprintln!("{}", update);
    }
}

fn main() -> Result<()> {
    tracer!(TRACE, "sq-wot");

    let cli = cli::Cli::parse();

    let mut policy = StandardPolicy::new();

    let known_notations: Vec<&str>
        = cli.known_notation.iter().map(|s| &s[..]).collect();
    policy.good_critical_notations(&known_notations[..]);

    let mut policy = ConfiguredStandardPolicy::from_policy(policy);
    policy.parse_default_config()?;

    let policy = &policy.build();

    // We check whether a trust root is specified here instead of
    // using Clap::ArgGroup, as we are able to provide the user with
    // more helpful feedback when that is not the case.
    if !(cli.trust_root.is_some()
         || cli.gossip
         || cli.gpg_ownertrust
         || cli.gpg
         // No trust root is needed for path.
         || matches!(cli.subcommand, cli::Subcommand::Path { .. }))
    {
        eprintln!("\
No trust roots specified.  Specify one or more trust roots using
`--trust-root` or use `--gpg-ownertrust` or `--gpg` to read the
trust roots from GnuPG, or use `--gossip` to discover what others
think.");
        std::process::exit(1);
    }

    // Likewise for keyrings.
    if !(cli.keyring.is_some()
         || cli.gpg_keyring || cli.gpg
         || cli.network)
    {
        eprintln!("\
No keyrings specified.  Specify one or more keyring using `--keyring`
or use `--gpg-keyring` or `--gpg` to read GnuPG's keyring, or use `--network`
to lookup certificates on key servers.  Note: `--network` may be used
alone or combined with the other options.");
        std::process::exit(1);
    }


    let mut trust_roots: Vec<(KeyHandle, usize)> = Vec::new();
    // If --gossip is present, ignore any trust roots.
    if ! cli.gossip {
        if let Some(ref roots) = cli.trust_root {
            for root in roots.into_iter().cloned() {
                trust_roots.push((root, wot::FULLY_TRUSTED));
            }
        }
    }

    let mut possible_roots: Vec<wot::Root> = Vec::new();

    if ! cli.gossip && (cli.gpg_ownertrust || cli.gpg) {
        let ownertrust = gpg::export_ownertrust()?;
        for (fpr, ownertrust) in ownertrust {
            match ownertrust {
                gpg::OwnerTrust::Ultimate => {
                    trust_roots.push((fpr.into(), wot::FULLY_TRUSTED));
                },
                gpg::OwnerTrust::Fully => {
                    possible_roots.push(
                        wot::Root::new(fpr, wot::FULLY_TRUSTED));
                },
                gpg::OwnerTrust::Marginal => {
                    possible_roots.push(
                        wot::Root::new(fpr, wot::PARTIALLY_TRUSTED));
                },
                _ => (),
            }
        }
    }

    let mut certs: Vec<RawCert> = Vec::new();
    if let Some(keyrings) = cli.keyring.as_ref() {
        for filename in keyrings {
            let some_certs = RawCertParser::from_file(filename)
                .context(format!("Parsing {:?}", filename))?
                .into_iter()
                .filter_map(|cert| {
                    match cert {
                        Ok(cert) => Some(cert),
                        Err(err) => {
                            eprintln!("Warning: while parsing {:?}: {}",
                                      filename, err);
                            None
                        }
                    }
                });

            certs.extend(some_certs);
        }
    }

    let keyring;
    if cli.gpg_keyring || cli.gpg {
        keyring = gpg::export()?;
        let some_certs = RawCertParser::from_bytes(&keyring[..])?
            .into_iter()
            .filter_map(|cert| {
                match cert {
                    Ok(cert) => Some(cert),
                    Err(err) => {
                        eprintln!("Warning: while parsing gpg's keyring: {}",
                                  err);
                        None
                    }
                }
            });

        certs.extend(some_certs);
    }

    // Map any Key IDs to Fingerprints.
    let mut keyid_index: Option<BTreeMap<KeyID, Vec<Fingerprint>>> = None;
    let mut trust_roots: Vec<wot::Root> = trust_roots
        .into_iter()
        .filter_map(|(kh, amount)| {
            match kh {
                KeyHandle::Fingerprint(fpr) => {
                    Some(wot::Root::new(fpr, amount))
                },
                KeyHandle::KeyID(keyid) => {
                    let i = match keyid_index.as_ref() {
                        Some(i) => i,
                        None => {
                            // Initialize the hash map lazily.
                            let mut i: BTreeMap<KeyID, Vec<Fingerprint>>
                                = BTreeMap::new();
                            for c in certs.iter() {
                                match i.entry(c.keyid()) {
                                    e @ Entry::Occupied(_) => {
                                        e.and_modify(|e| {
                                            e.push(c.fingerprint())
                                        });
                                    }
                                    e @ Entry::Vacant(_) => {
                                        e.or_insert(
                                            vec![ c.fingerprint() ]);
                                    }
                                }
                            }

                            keyid_index = Some(i);
                            keyid_index.as_ref().unwrap()
                        }
                    };

                    if let Some(fprs) = i.get(&keyid) {
                        if fprs.len() > 1 {
                            let fprs: Vec<String>
                                = fprs.iter().map(|f| f.to_hex()).collect();
                            eprintln!("\
Key ID ({}) matches multiple certificates: {}",
                                      keyid, fprs.join(", "));
                            std::process::exit(1);
                        }
                        Some(wot::Root::new(fprs[0].clone(), amount))
                    } else {
                        t!("Ignoring root {}: not in network.", keyid);
                        None
                    }
                }
            }
        })
        .collect();

    let reference_time = if let Some(ref t) = cli.time {
        let time = SystemTime::from(
            crate::parse_iso8601(
                &t, chrono::NaiveTime::from_hms_opt(0, 0, 0).expect("valid"))
                .context(format!("Parsing --time {}", t))?);
        time
    } else {
        SystemTime::now()
    };

    let backend = cert_store::store::Certs::from_certs(certs.into_iter())?;

    let store: Box<dyn StorePlusBackend> = if cli.network {
        // This opens the default cert-d in read/write mode, which is
        // what we want: we want keyserver lookups to be cached
        // locally.
        let mut cert_store = cert_store::CertStore::new()?;

        // Add the user data as a read-only overlay.
        cert_store.add_backend(Box::new(backend), AccessMode::Always);

        // And configure the key server.
        let mut ks = KeyServer::new(&cli.keyserver)?;
        ks.add_listener(Box::new(KeyServerUpdate { }));

        cert_store.add_keyserver_backend(ks)?;

        // Turn it into a WoT-compatible CertStore.
        let cert_store = CertStore::from_store(
            cert_store, policy, reference_time);

        Box::new(cert_store)
    } else {
        let cert_store = CertStore::from_store(
            backend, policy, reference_time);
        Box::new(cert_store)
    };

    // Precompute as much as possible if we do a list.  Otherwise, we
    // compute on demand.
    let precompute = matches!(&cli.subcommand, cli::Subcommand::List { .. });

    if precompute {
        store.precompute();
    }

    let roots = wot::Roots::new(trust_roots.iter().cloned());
    let mut n = wot::NetworkBuilder::rooted(&store, roots);
    if cli.certification_network {
        n = n.certification_network();
    };
    let mut n = n.build();

    let mut found_one = true;
    while found_one && ! possible_roots.is_empty() {
        // For GnuPG to consider a non-ultimately trusted root as
        // valid, there must be a path from an ultimately trusted root
        // to the non-ultimately trusted root.  If this is the case,
        // add those roots.

        t!("Checking if any of {} are reachable from the current {} roots",
           possible_roots.iter()
               .fold(String::new(), |mut s, r| {
                   if ! s.is_empty() {
                       s.push_str(", ");
                   }
                   s.push_str(&r.fingerprint().to_hex());
                   s
               }),
           trust_roots.len());

        found_one = false;
        let pr = possible_roots;
        possible_roots = Vec::new();

        'root: for other_root in pr.into_iter() {
            let cert = match n.lookup_synopsis_by_fpr(other_root.fingerprint()) {
                Err(_err) => {
                    t!("Ignoring root {}: not in network.",
                       other_root.fingerprint());
                    continue;
                }
                Ok(cert) => cert,
            };

            for u in cert.userids() {
                if u.revocation_status().in_effect(reference_time) {
                    t!("Ignoring root {}'s User ID {:?}: revoked.",
                       other_root.fingerprint(),
                       String::from_utf8_lossy(u.value()));
                    continue;
                }

                let authenticated_amount
                    = n.authenticate(
                        u.userid(), other_root.fingerprint(),
                        wot::FULLY_TRUSTED)
                    .amount();

                if authenticated_amount >= wot::FULLY_TRUSTED {
                    // Authenticated!  We'll keep it.
                    t!("Non-ultimately trusted root <{}, {}> reachable, \
                        keeping at {}",
                       other_root.fingerprint(),
                       String::from_utf8_lossy(u.userid().value()),
                       other_root.amount());
                    found_one = true;

                    trust_roots.push(other_root);
                    let roots = wot::Roots::new(trust_roots.clone());
                    let mut builder = wot::NetworkBuilder::rooted(&store, roots);
                    if cli.certification_network {
                        builder = builder.certification_network();
                    };
                    n = builder.build();

                    continue 'root;
                } else {
                    t!("Non-ultimately trusted binding <{}, {}> \
                        NOT fully trusted (amount: {})",
                       other_root.fingerprint(),
                       String::from_utf8_lossy(u.userid().value()),
                       authenticated_amount);
                }
            }

            t!("Non-ultimately trusted root {} NOT fully trusted. Ignoring.",
               other_root.fingerprint());
            possible_roots.push(other_root);
        }
    }

    match &cli.subcommand {
        cli::Subcommand::Authenticate { cert, userid, .. } => {
            // Authenticate a given binding.
            authenticate(&cli, &n, cli.gossip, Some(userid), Some(cert))?;
        }
        cli::Subcommand::Lookup { userid, .. } => {
            // Find all authenticated bindings for a given
            // User ID, list the certificates.
            authenticate(&cli, &n, cli.gossip, Some(userid), None)?;
        }
        cli::Subcommand::Identify { cert, .. } => {
            // Find and list all authenticated bindings for a given
            // certificate.
            authenticate(&cli, &n, cli.gossip, None, Some(cert))?;
        }
        cli::Subcommand::List { .. } => {
            // List all authenticated bindings.
            authenticate(&cli, &n, cli.gossip, None, None)?;
        }
        cli::Subcommand::Path { .. } => {
            check_path(&cli, &n, policy)?;
        }
    }

    Ok(())
}
