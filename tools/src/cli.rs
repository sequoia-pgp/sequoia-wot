/// Command-line parser for sq-wot.

use std::path::PathBuf;
use std::ops::Deref;

use clap::{Parser, ValueEnum};

use crate::openpgp;
use openpgp::KeyHandle;
use openpgp::Result;
use openpgp::packet::UserID;

pub const DEFAULT_OUTPUT_FORMAT: &str = "human-readable";
pub const KEYSERVER_DEFAULT: &'static str = "hkps://keyserver.ubuntu.com";

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
/// Supported output types
pub enum OutputFormat {
    #[cfg(feature = "dot-writer")]
    /// output in graphviz's DOT format
    Dot,
    /// output in human readable format
    HumanReadable,
}

/// A command-line frontend for Sequoia's web-of-trust engine.
///
/// This program presents a CLI to query a web-of-trust network.
///
/// Functionality is grouped and available using subcommands.
/// Currently, this interface is stateless.  Therefore, you need to
/// supply all configuration and certificates explicitly on each
/// invocation.
///
/// OpenPGP data can be provided in binary or ASCII armored form.  This
/// will be handled automatically.
///
/// We use the term `certificate`, or cert for short, to refer to
/// OpenPGP keys that do not contain secret key material.  We reserve
/// the term `key` for OpenPGP certificates that also contain secret
/// key material.
///
/// By default, `sq-wot` reads a cryptographic configuration policy
/// from `/etc/crypto-policies/back-ends/sequoia.config`.  This
/// behavior can be overridden by setting the environment variable
/// `SEQUOIA_CRYPTO_POLICY` to the absolute path of an alternate
/// configuration file to read, or to the empty string to force
/// `sq-wot` to use the default policy.  The syntax of the
/// configuration file is described in the [`sequoia-policy-config`
/// crate's documentation].
///
/// [`sequoia-policy-config` crate's documentation]: https://docs.rs/sequoia-policy-config/latest/sequoia_policy_config/
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    /// Uses gpg's keyring and gpg's trust roots.
    ///
    /// When this option is set, `sq-wot` reads gpg's keyring and
    /// gpg's ownertrust.  This is equivalent to passing
    /// `--gpg-keyring` and `--gpg-ownertrust`.
    #[arg(global=true, long)]
    pub gpg: bool,

    /// Adds KEYRING to the list of keyrings
    ///
    /// The keyrings are read at start up and used to build a web of
    /// trust network.  Note: if a certificate occurs multiple times,
    /// the first version is taken; they are not currently merged.
    #[arg(global=true, short='k', long, value_name="FILE")]
    pub keyring: Option<Vec<PathBuf>>,

    /// Adds GnuPG's keyring to the list of keyrings.
    ///
    /// This option causes `sq-wot` to read gpg's keyring, by parsing
    /// the output of `gpg --export --export-options
    /// export-local-sigs`.
    #[arg(global=true, long)]
    pub gpg_keyring: bool,

    /// Looks up missing certificates over the network.
    ///
    /// This causes `sq-wot` to look up missing certificates on a key
    /// server.  The default key server can be overridden using the
    /// `--keyserver` option.
    ///
    /// Certificates fetched from a key server are cached locally in
    /// the default cert-d.  The default cert-d is also checked prior
    /// to fetching a certificate from the key server.
    #[arg(global=true, long)]
    pub network: bool,

    /// Sets the keyserver to use to KEYSERVER.
    ///
    /// This option only makes sense when used in conjunction with the
    /// `--network` option.  Currently, it is only possible to set a
    /// single keyserver.
    #[arg(global=true, long, default_value=KEYSERVER_DEFAULT)]
    pub keyserver: String,


    /// Treats the specified certificate as a trust root.
    ///
    /// It is possible to have multiple trust roots.  All trust roots
    /// are treated equivalently.  This can be combined with
    /// `--gpg-ownertrust`.
    #[arg(global=true, short='r', long, value_name="FINGERPRINT|KEYID")]
    pub trust_root: Option<Vec<KeyHandle>>,

    /// Render the output in a specific format
    ///
    /// Choosing a different output format allows for further post
    /// processing of the data using external tools.
    #[
        arg(
            value_enum,
            global=true,
            short='f',
            long,
            default_value=DEFAULT_OUTPUT_FORMAT,
            value_name="FORMAT",
        )
    ]
    pub format: OutputFormat,

    /// Causes `sq-wot` to use gpg's trust roots as the trust roots.
    ///
    /// `sq-wot` reads the output of `gpg --export-ownertrust`.  It
    /// treats gpg's ultimately trusted certificates as fully trust
    /// roots.  Similar to gpg, it also treats certificates marked as
    /// fully and marginally trusted as fully and marginally trusted
    /// roots, if a self-signed User ID can be authenticated by an
    /// ultimately trusted root.
    ///
    /// It is possible to set additional trust roots using the
    /// `--trust-root` option.
    #[arg(global=true, long)]
    pub gpg_ownertrust: bool,

    /// Treats all certificates as unreliable trust roots.
    ///
    /// This option is useful for figuring out what others think about
    /// a certificate (i.e., gossip or hearsay).  In other words, this
    /// finds arbitrary paths to a particular certificate.
    ///
    /// Gossip is useful in helping to identify alternative ways to
    /// authenticate a certificate.  For instance, imagine Ed wants to
    /// authenticate Laura's certificate, but asking her directly is
    /// inconvenient.  Ed discovers that Micah has certified Laura's
    /// certificate, but Ed hasn't yet authenticated Micah's
    /// certificate.  If Ed is willing to rely on Micah as a trusted
    /// introducer, and authenticating Micah's certificate is easier
    /// than authenticating Laura's certificate, then Ed has learned
    /// about an easier way to authenticate Laura's certificate.
    ///
    /// EXAMPLES:
    ///
    /// # Get gossip about a certificate.{n}
    /// $ sq-wot --keyring keyring.pgp \\{n}
    ///     --gossip identify 3217C509292FC67076ECD75C7614269BDDF73B36
    #[arg(global=true, long)]
    pub gossip: bool,


    /// Treats the network as a certification network.
    ///
    /// Normally, `sq-wot` treats the web-of-trust network as an
    /// authentication network where a certification only means that
    /// the binding is correct, not that the target should be treated
    /// as a trusted introducer.  In a certification network, the
    /// targets of certifications are treated as trusted introducers
    /// with infinite depth, and any regular expressions are ignored.
    /// Note: The trust amount remains unchanged.  This is how most
    /// so-called pgp path-finding algorithms work.
    #[arg(global=true, long)]
    pub certification_network: bool,

    /// The required amount of trust.
    ///
    /// 120 indicates full authentication; values less than 120
    /// indicate partial authentication.  When
    /// `--certification-network` is passed, this defaults to 1200,
    /// i.e., sq-wot tries to find 10 paths.
    #[arg(global=true, short='a', long,
          conflicts_with_all=["partial", "full", "double"])]
    pub trust_amount: Option<usize>,

    /// Require partial authentication.
    ///
    /// This is the same as passing `--trust-amount 40`.
    #[arg(global=true, long,
          conflicts_with_all=["trust_amount", "full", "double"])]
    pub partial: bool,

    /// Require full authentication.
    ///
    /// This is the same as passing `--trust-amount 120`.
    #[arg(global=true, long,
          conflicts_with_all=["trust_amount", "partial", "double"])]
    pub full: bool,

    /// Require double authentication.
    ///
    /// This is the same as passing `--trust-amount 240`.
    #[arg(global=true, long,
          conflicts_with_all=["trust_amount", "partial", "full"])]
    pub double: bool,


    /// Sets the reference time to TIME.
    ///
    /// TIME is interpreted as an ISO 8601 timestamp.  To set the
    /// reference time to July 21, 2013 at midnight UTC, you can
    /// do:
    ///
    ///   $ sq-wot --time 20130721 CMD ...
    ///
    /// To include a time, add a T, the time and optionally the
    /// timezone (the default timezone is UTC):
    ///
    ///   $ sq-wot --time 20130721T0550+0200 CMD ...
    #[arg(global=true, long)]
    pub time: Option<String>,

    /// Adds NOTATION to the list of known notations
    ///
    /// This is used when validating signatures.  Signatures that have
    /// unknown notations with the critical bit set are considered
    /// invalid.
    #[arg(global=true, long)]
    pub known_notation: Vec<String>,


    #[command(subcommand)]
    pub subcommand: Subcommand,
}

#[derive(clap::Subcommand)]
pub enum Subcommand {
    /// Authenticate a binding.
    ///
    /// Authenticate a binding (a certificate and User ID) by looking
    /// for a path from the trust roots to the specified binding in
    /// the web of trust.  Because certifications may express
    /// uncertainty (i.e., certifications may be marked as conveying
    /// only partial or marginal trust), multiple paths may be needed.
    ///
    /// If a binding could be authenticated to the specified level (by
    /// default: fully authenticated, i.e., a trust amount of 120),
    /// then the exit status is 0.  Otherwise the exit status is 1.
    ///
    /// If any valid paths to the binding are found, they are printed
    /// on stdout whether they are sufficient to authenticate the
    /// binding or not.
    #[command(after_help("\
EXAMPLES:

  # Authenticate a binding.
  $ sq-wot --keyring keyring.pgp \\
      --partial \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    authenticate \\
      C7966E3E7CE67DBBECE5FC154E2AD944CFC78C86 \\
      'Alice <alice@example.org>'

  # The same as above, but this time generate output in DOT format
  # and convert it to an SVG using Graphviz's DOT compiler.
  $ sq-wot --format dot \\
      --keyring keyring.pgp \\
      --partial \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    authenticate \\
      C7966E3E7CE67DBBECE5FC154E2AD944CFC78C86 \\
      'Alice <alice@example.org>' \\
    | dot -Tsvg -o alice.pgp

  # Try and authenticate each binding where the User ID has the
  # specified email address.
  $ sq-wot --keyring keyring.pgp \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    authenticate \\
      C7966E3E7CE67DBBECE5FC154E2AD944CFC78C86 \\
      --email 'alice@example.org'

  # The same as above, but this time generate output in DOT format
  # and convert it to an SVG using Graphviz's DOT compiler.
  $ sq-wot --format dot \\
      --keyring keyring.pgp \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    authenticate \\
      C7966E3E7CE67DBBECE5FC154E2AD944CFC78C86 \\
      --email 'alice@example.org' \\
    | dot -Tsvg -o alice.svg
"))]
    Authenticate {
        #[command(flatten)]
        email: EmailArg,

        #[command(flatten)]
        cert: CertArg,

        #[command(flatten)]
        userid: UserIDArg,
    },

    /// Lookup the certificates associated with a User ID.
    ///
    /// Identifies authenticated bindings (User ID and certificate
    /// pairs) where the User ID matches the specified User ID.
    ///
    /// If a binding could be authenticated to the specified level (by
    /// default: fully authenticated, i.e., a trust amount of 120),
    /// then the exit status is 0.  Otherwise the exit status is 1.
    ///
    /// If a binding could be partially authenticated (i.e., its trust
    /// amount is greater than 0), then the binding is displayed, even
    /// if the trust is below the specified threshold.
    #[command(after_help("\
EXAMPLES:

  # Lookup a certificate with the given User ID.
  $ sq-wot --keyring keyring.pgp \\
      --partial \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    lookup \\
      'Alice <alice@example.org>'

  # The same as above, but output in DOT format and convert it to
  # an SVG using Graphviz's DOT compiler.
  $ sq-wot --format dot \\
      --keyring keyring.pgp \\
      --partial \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    lookup \\
      'Alice <alice@example.org>' \\
    | dot -Tsvg -o alice.svg

  # Lookup a certificate with the given email address.
  $ sq-wot --keyring keyring.pgp \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    lookup \\
      --email 'alice@example.org'

  # The same as above, but output in DOT format and convert it to
  # an SVG using Graphviz's DOT compiler.
  $ sq-wot --format dot \\
      --keyring keyring.pgp \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    lookup \\
      --email 'alice@example.org' \\
    | dot -Tsvg -o alice.svg
"))]
    Lookup {
        #[command(flatten)]
        email: EmailArg,

        #[command(flatten)]
        userid: UserIDArg,
    },

    /// Identify a certificate.
    ///
    /// Identify a certificate by finding authenticated bindings (User
    /// ID and certificate pairs).
    ///
    /// If a binding could be authenticated to the specified level (by
    /// default: fully authenticated, i.e., a trust amount of 120),
    /// then the exit status is 0.  Otherwise the exit status is 1.
    ///
    /// If a binding could be partially authenticated (i.e., its trust
    /// amount is greater than 0), then the binding is displayed, even
    /// if the trust is below the specified threshold.
    #[command(after_help("\
EXAMPLES:

  # Identify a certificate.
  $ sq-wot --keyring keyring.pgp \\
      --partial \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    identify \\
      C7B1406CD2F612E9CE2136156F2DA183236153AE

  # The same as above, but output in DOT format and convert it to
  # an SVG using Graphviz's DOT compiler.
  $ sq-wot --format dot \\
      --keyring keyring.pgp \\
      --partial \\
      --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    identify \\
      C7B1406CD2F612E9CE2136156F2DA183236153AE \\
    | dot -Tsvg -o C7B1406CD2F612E9CE2136156F2DA183236153AE.svg
"))]
    Identify {
        #[command(flatten)]
        cert: CertArg,
    },

    /// List all authenticated bindings (User ID and certificate
    /// pairs).
    ///
    /// Only bindings that meet the specified trust amount (by default
    /// bindings that are fully authenticated, i.e., have a trust
    /// amount of 120), are shown.
    ///
    /// Even if no bindings are shown, the exit status is 0.
    ///
    /// If --email is provided, then a pattern matches if it is a case
    /// insensitive substring of the email address as-is or the
    /// normalized email address.  Note: unlike the email address, the
    /// pattern is not normalized.  In particular, puny code
    /// normalization is not done on the pattern.
    #[command(after_help("\
EXAMPLES:

# List all bindings for example.org that are at least partially
# authenticated.
$ sq-wot --keyring keyring.pgp \\
    --partial \\
    --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
  list @example.org

# The same as above, but output in DOT format and convert it to
# an SVG using Graphviz's DOT compiler.
$ sq-wot --format dot \\
    --keyring keyring.pgp \\
    --partial \\
    --trust-root 8F17777118A33DDA9BA48E62AACB3243630052D9 \\
  list @example.org \\
  | dot -Tsvg -o example_org.svg
"))]
    List {
        #[command(flatten)]
        email: EmailArg,

        /// A pattern to select the bindings to authenticate.
        ///
        /// The pattern is treated as a UTF-8 encoded string and a
        /// case insensitive substring search (using the current
        /// locale) is performed against each User ID.  If a User ID
        /// is not valid UTF-8, the binding is ignored.
        pattern: Option<String>,
    },
    /// Verify the specified path.
    ///
    /// A path is a sequence of certificates starting at the root, and
    /// a User ID.  This function checks that each path segment has a
    /// valid certification, which also satisfies any constraints
    /// (trust amount, trust depth, regular expressions).
    ///
    /// If a valid path is not found, then this subcommand also lints
    /// the path.  In particular, it report if any certifications are
    /// insufficient, e.g., not enough trust depth, or invalid, e.g.,
    /// because they use SHA-1, but the use of SHA-1 has been
    /// disabled.
    #[command(after_help("\
EXAMPLES:

# Verify that Neal ceritified Justus's certificate for a particular User ID.
$ sq-wot --keyring keyring.pgp \\
  path \\
    8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    CBCD8F030588653EEDD7E2659B7DD433F254904A \\
    \"Justus Winter <justus@sequoia-pgp.org>\"

# The same as above, but output in DOT format and convert it to
# an SVG using Graphviz's DOT compiler.
$ sq-wot --format dot \\
    --keyring keyring.pgp \\
  path \\
    8F17777118A33DDA9BA48E62AACB3243630052D9 \\
    CBCD8F030588653EEDD7E2659B7DD433F254904A \\
    \"Justus Winter <justus@sequoia-pgp.org>\" \\
  | dot -Tsvg -o neal--justus.svg
"))]
    Path {
        #[command(flatten)]
        email: EmailArg,

        // This should actually be a repeatable positional argument
        // (Vec<Cert>) followed by a manadatory positional argument (a
        // User ID), but that is not allowed by Clap v3 and Clap v4
        // even though it worked fine in Clap v2.  (Curiously, it
        // works in `--release` mode fine and the only error appears
        // to be one caught by a `debug_assert`).
        //
        // https://github.com/clap-rs/clap/issues/3281
        #[command(flatten)]
        path: PathArg,
    },
}

impl Subcommand {
    pub fn email(&self) -> bool {
        use Subcommand::*;

        match self {
            Authenticate { email, .. } => email.email,
            Lookup { email, .. } => email.email,
            Identify { .. } => false,
            Path { email, .. } => email.email,
            List { email, .. } => email.email,
        }
    }
}

#[derive(clap::Args, Debug)]
pub struct CertArg {
    /// The fingerprint or Key ID of the certificate to authenticate.
    #[arg(value_name="FINGERPRINT|KEYID")]
    cert: KeyHandle
}

impl Deref for CertArg {
    type Target = KeyHandle;

    fn deref(&self) -> &Self::Target {
        &self.cert
    }
}

#[derive(clap::Args, Debug)]
pub struct PathArg {
    /// A path consists of one or more certificates (designated by
    /// their fingerprint or Key ID) and ending in the User ID that is
    /// being authenticated.
    #[arg(value_names=["FINGERPRINT|KEYID", "USERID"])]
    elements: Vec<String>,
}

const PATH_DESC: &str = "\
A path consists of one or more certificates (identified by their
respective fingerprint or Key ID) and a User ID.";

impl PathArg {
    fn check(&self) -> Result<()> {
        if self.elements.len() < 2 {
            Err(anyhow::anyhow!(
"\
The following required arguments were not provided:
  {}<USERID>

{}

Usage: sq-wot path <FINGERPRINT|KEYID>... <USERID>

For more information try '--help'",
                if self.elements.len() == 0 {
                    "<FINGERPRINT|KEYID>\n  "
                } else {
                    ""
                },
                PATH_DESC))
        } else {
            Ok(())
        }
    }

    pub fn certs(&self) -> Result<Vec<KeyHandle>> {
        self.check()?;

        // Skip the last one.  That's the User ID.
        self.elements[0..self.elements.len() - 1]
            .iter()
            .map(|e| {
                e.parse()
                    .map_err(|err| {
                        anyhow::anyhow!(
"Invalid value {:?} for '<FINGERPRINT|KEYID>': {}

{}

For more information try '--help'",
                            e, err, PATH_DESC)
                    })
            })
            .collect::<Result<Vec<KeyHandle>>>()
    }

    pub fn userid(&self) -> Result<UserID> {
        self.check()?;

        let userid = self.elements.last().expect("just checked");
        Ok(UserID::from(userid.as_bytes()))
    }
}

#[derive(clap::Args, Debug)]
pub struct UserIDArg {
    /// The User ID to authenticate.
    ///
    /// This is case sensitive, and must be the whole User ID, not
    /// just a substring or an email address.
    pub userid: UserID,
}

impl Deref for UserIDArg {
    type Target = UserID;

    fn deref(&self) -> &Self::Target {
        &self.userid
    }
}

#[derive(clap::Args, Debug)]
pub struct EmailArg {
    /// Changes the USERID parameter to match User IDs with the
    /// specified email address.
    ///
    /// Interprets the USERID parameter as an email address, which
    /// is then used to select User IDs with that email address.
    ///
    /// Unlike when comparing User IDs, email addresses are first
    /// normalized by the domain to ASCII using IDNA2008 Punycode
    /// conversion, and then converting the resulting email
    /// address to lowercase using the empty locale.
    ///
    /// If multiple User IDs match, they are each considered in
    /// turn, and this function returns success if at least one of
    /// those User IDs can be authenticated.  Note: The paths to
    /// the different User IDs are not combined.
    #[arg(long)]
    pub email: bool,
}

impl Deref for EmailArg {
    type Target = bool;

    fn deref(&self) -> &Self::Target {
        &self.email
    }
}

