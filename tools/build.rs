use std::fs;
use std::io;

fn main() {
    build_man_pages().unwrap();
    build_shell_completions().unwrap();
}

// We include cli.rs, which depends on a few data structures from
// sequoia_openpgp.  To avoid adding sequoia_openpgp as a build
// dependency, we provide stubs for the functionality that cli.rs
// uses.
mod openpgp {
    pub type Result<R, E=anyhow::Error> = anyhow::Result<R, E>;

    pub mod packet {
        use super::*;

        #[derive(Clone, Debug)]
        pub struct UserID {}

        impl From<&[u8]> for UserID {
            fn from(_: &[u8]) -> Self {
                UserID {}
            }
        }

        impl std::str::FromStr for UserID {
            type Err = anyhow::Error;

            fn from_str(_: &str) -> Result<Self> {
                Ok(UserID {})
            }
        }
    }

    #[derive(Clone, Debug)]
    pub struct KeyHandle {
    }

    impl std::str::FromStr for KeyHandle {
        type Err = anyhow::Error;

        fn from_str(_: &str) -> Result<Self> {
            Ok(KeyHandle {})
        }
    }
}

#[allow(unused)]
mod cli {
    include!("src/cli.rs");
}

fn build_man_pages() -> io::Result<()> {
    // Man page support.
    let out_dir = std::path::PathBuf::from(
        std::env::var_os("OUT_DIR").ok_or(std::io::ErrorKind::NotFound)?);

    use clap::CommandFactory;

    let man = clap_mangen::Man::new(cli::Cli::command());
    let mut buffer: Vec<u8> = Default::default();
    man.render(&mut buffer)?;

    let filename = out_dir.join("sq-wot.1");
    println!("cargo:warning=writing man page to {}", filename.display());
    std::fs::write(filename, buffer)?;

    for sc in cli::Cli::command().get_subcommands() {
        let man = clap_mangen::Man::new(sc.clone());
        let mut buffer: Vec<u8> = Default::default();
        man.render(&mut buffer)?;

        let filename = out_dir.join(format!("sq-wot-{}.1", sc.get_name()));
        println!("cargo:warning=writing man page to {}", filename.display());
        std::fs::write(filename, buffer)?;
    }

    Ok(())
}

fn build_shell_completions() -> io::Result<()> {
    use clap_complete::Shell;

    let out_dir = std::path::PathBuf::from(
        std::env::var_os("OUT_DIR").ok_or(std::io::ErrorKind::NotFound)?);

    fs::create_dir_all(&out_dir).unwrap();

    use clap::CommandFactory;

    let mut sq_wot = cli::Cli::command();
    for shell in &[Shell::Bash, Shell::Fish, Shell::Zsh, Shell::PowerShell,
                   Shell::Elvish] {
        let path = clap_complete::generate_to(
            *shell, &mut sq_wot, "sq-wot", &out_dir)
            .unwrap();
        println!("cargo:warning=completion file is generated: {:?}", path);
    };

    Ok(())
}
