# Sequoia Web of Trust

A Rust library and tool for authenticating bindings between User IDs
and certificates using OpenPGP's web of trust.  This library is
designed around OpenPGP's data structures, but it does not require
OpenPGP data.  Instead, it is possible to manually describe a web of
trust.
