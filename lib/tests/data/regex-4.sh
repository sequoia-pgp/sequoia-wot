#! /bin/bash

set -e

. gen-helper.sh --directory=regex-4 ${@:+"$@"}

key alice@example.org
key bob@other.org
key carol@some.org
key dave@other.org

certify alice -d 1 --domain=some.org bob@other.org
certify bob carol@some.org
certify bob dave@other.org

finish
