#! /bin/bash

. gen-helper.sh --directory=userid-revoked ${@:+"$@"}

set -e

t0=20200101
t1=20200201
t2=20200301
t3=20200401

key -t $t0 alice
key -t $t0 bob
key -t $t0 carol

set -x

certify -t $t1 alice -a 60 -d 2 bob
certify -t $t1 bob -a 60 -d 1 carol

certify -t $t3 alice -a 90 -d 2 bob
certify -t $t3 bob -a 90 -d 1 carol

revoke -t $t2 -u '<bob@example.org>' bob retired

finish
