#! /bin/bash

set -e

. gen-helper.sh --directory=multiple-userids-3 ${@:+"$@"}

key alice
key bob@some.org bob@other.org bob@third.org
key carol
key dave
key ed
key frank

certify alice -a 40 -d 2 bob@some.org
certify alice -a 30 -d 3 bob@other.org
certify bob -a 20 -d 1 carol
certify bob -a 120 -d 2 dave
certify carol -a 120 frank
certify dave -a 120 -d 1 ed
certify ed -a 120 frank

finish
