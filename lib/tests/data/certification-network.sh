#! /bin/bash

. gen-helper.sh --directory=certification-network ${@:+"$@"}

key alice
key bob
key carol
key dave

certify alice bob
certify bob carol
certify carol dave
certify -a 60 dave alice

finish
