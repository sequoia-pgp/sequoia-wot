Check that an expired certificate can't be authenticated and can't be
used to authenticate other certificates.

 t0: Create A, B, C
 t1: Create certifications (amount = 60)
 t2: B expires.
 t3: Create certifications (amount = 120)

```
  A
  | 1/60
  B
  | 1/60
  C
```

At t3, the new certifications are ignored, because they were created
after B expired.

At t3, B can still be used as a trusted introducer for C, because the
initial certifications were created before it expired, but it is no
longer possible to authenticate B.
