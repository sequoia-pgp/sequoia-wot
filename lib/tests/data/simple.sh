#! /bin/bash

. gen-helper.sh --directory=simple ${@:+"$@"}

key alice
key bob
key carol
key dave
key ellen
key frank

certify alice -a 100 -d 2 bob
certify bob -a 100 -d 1 carol
certify carol -a 100 -d 1 dave
certify dave -a 100 -d 1 ellen

finish
