```text
    Alice <alice@example.org>
    |
    | Authorization (depth: 1, amount: 120),
    | Regular expression: some.org
    v
    Bob <bob@other.org>
   /                   \
  / Certification       \ Certification
v                        v
Carol <carol@some.org>   Dave <dave@other.org>
```

Alice delegates to Bob, but only for "some.org".  Bob's email address
is not in some.org, but that shouldn't matter.  He should still be
able to introduce Carol, because her email is in "some.org".
