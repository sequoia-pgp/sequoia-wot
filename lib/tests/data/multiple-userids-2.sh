#! /bin/bash

set -e

. gen-helper.sh --directory=multiple-userids-2 ${@:+"$@"}

key alice
key bob@some.org bob@other.org
key carol
key dave@other.org
key ed@example.org
key frank@other.org

certify alice -a 50 -d 1 bob@some.org
certify alice -a 70 -d 255 --domain other.org bob@other.org
certify bob -a 120 -d 2 carol
certify bob -a 120 frank@other.org
certify carol -a 120 dave@other.org
certify carol -a 120 ed@example.org

finish
