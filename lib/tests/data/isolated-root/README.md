If a root is isolated, make sure we can still answer queries about it.

  - t0: A is created
  - t1: A's User ID is revoked.
