#! /bin/bash

. gen-helper.sh --directory=cert-revoked-hard ${@:+"$@"}

t0=20200101
t1=20200201
t2=20200301
t3=20200401

set -x

key -t $t0 alice
key -t $t0 bob
key -t $t0 carol
key -t $t0 dave

certify -t $t1 alice -a 90 -d 2 bob
certify -t $t1 bob -a 60 -d 1 dave
certify -t $t1 alice -a 30 -d 2 carol
certify -t $t1 carol -a 120 -d 1 dave

certify -t $t3 alice -a 120 -d 1 bob
certify -t $t3 bob -a 120 -d 1 dave

revoke -t $t2 bob compromised

finish
