#! /bin/bash

. gen-helper.sh --directory=trivial ${@:+"$@"}

key alice@example.org
key ca@some.org
key bob@some.org
key carol@other.org

certify alice -a 40 -d 1 --domain some.org ca@some.org
certify ca -a 120 bob@some.org
certify ca -a 120 carol@other.org

finish
