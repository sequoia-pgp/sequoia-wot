#! /bin/bash

. gen-helper.sh --directory=my-own-grandpa ${@:+"$@"}

key my-own-grandpa

FPR=$(sq keyring list "my-own-grandpa/my-own-grandpa-priv.pgp" | awk '{ print $2 }')

sq key subkey bind --keyring "my-own-grandpa/my-own-grandpa-priv.pgp" \
   --key "$FPR" \
   --cert-file "my-own-grandpa/my-own-grandpa-priv.pgp" \
   --output - \
   > "my-own-grandpa/my-own-grandpa.pgp"

finish
