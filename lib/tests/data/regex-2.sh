#! /bin/bash

set -e

. gen-helper.sh --directory=regex-2 ${@:+"$@"}

key alice@some.org
key bob@some.org
key carol@other.org
key dave@their.org
key ed@example.org

certify alice -a 100 -d 7 bob@some.org
certify bob -a 100 -d 7 --domain=example.org carol@other.org
certify carol -a 100 -d 7 dave@their.org
certify dave -a 100 -d 7 ed@example.org

finish
