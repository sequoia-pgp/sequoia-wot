In this test Alice has certified two different User IDs for Bob.
First, we check that at most one of those certifications is used.
Then we check that both are considered.  Because neither certification
is better than the other (one has a larger trust amount; the other has
more depth), different scenarios will result in different
certifications being selected.


Alice has certified two of Bob's User IDs.  One with a trust amount of
50 and depth 2 and the other with a trust amount of 70 and depth 1.

Using Alice as a root and authenticating Carol, we can get a trust
amount of 70.  Although Bob - Carol has a capacity of 120, we only use
one User ID per key.

When authenticating Dave, we get a trust amount of 50.  This is
because the delegation with a trust amount of 70 does not have enough
depth to reach dave so we use the other certification.

```
                alice
       50/2   /       \ 70/1
  bob@some.org - bob - bob@other.org
                  | 120/2
                carol
                  | 120
                dave
```
