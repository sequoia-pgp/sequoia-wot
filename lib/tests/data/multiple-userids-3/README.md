```
               a
      40/2  /     \ 30/3           \ 10/255
bob@some.org - b - bob@other.org   bob@third.org
        20/1 /   \ 120/2
            c     d
            |     | 120/1
        120 |     e
             \   / 120
               f
```

The first time back propagation is run, the algorithm will find the
path a - b - c - f (b prefers c - f to d - e - f, because the former
is shorter).  The second time it is run, it will find a - b - d - e -
f.  The path's trust amount will be 10, because we suppress 20 between
a and b, and we can't use the bob@some.org certification as it doesn't
not have enough depth.
