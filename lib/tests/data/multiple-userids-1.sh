#! /bin/bash

set -e

. gen-helper.sh --directory=multiple-userids-1 ${@:+"$@"}

key alice
key bob@some.org bob@other.org
key carol
key dave@other.org

certify alice -a 50 -d 2 bob@some.org
certify alice -a 70 -d 1 bob@other.org
certify bob -a 120 -d 2 carol
certify carol -a 120 dave@other.org

finish
